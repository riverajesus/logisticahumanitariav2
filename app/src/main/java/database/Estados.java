package database;

import java.io.Serializable;

public class Estados implements Serializable {

    private String nombre;
    private int id_estado;

    public Estados() {
        this.nombre = "";
        this.id_estado = 0;
    }

    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public int getId_estado() { return id_estado; }
    public void setId_estado(int id_estado) { this.id_estado = id_estado; }
}
