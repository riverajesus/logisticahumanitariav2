package com.example.logisticahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothClass;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;

import database.EstadoPersistencia;
import database.Estados;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView edtNombre;
    Button btnGuardar, btnListar, btnLimpiar;
    Estados savedEstado;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGuardar = findViewById(R.id.btnGuardar);
        btnListar = findViewById(R.id.btnListar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        edtNombre = findViewById(R.id.edtNombre);
        setEvents();
        agregarDatos();
    }

    public void setEvents() {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGuardar:

                if (edtNombre.getText().toString().equals("")) {
                    edtNombre.setError("Introduce el Nombre");
                }
                if (edtNombre.getText().toString().equals("")) {
                    Toast.makeText(this, "Agregue datos", Toast.LENGTH_SHORT).show();
                }else {
                    EstadoPersistencia source = new EstadoPersistencia(MainActivity.this);
                    source.openDatabase();
                    Estados estados = new Estados();
                    estados.setNombre(edtNombre.getText().toString());
                    if(savedEstado==null){
                        source.insertContacto(estados);
                        Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                        limpiar();
                    }else{
                      source.updateContacto(estados, id);
                        Toast.makeText(MainActivity.this,R.string.mensajeedit, Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    source.close();
                }
                break;

            case R.id.btnLimpiar:
                limpiar();
                break;

            case R.id.btnListar:
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                limpiar();
                startActivityForResult(i, 0);
                break;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(Activity.RESULT_OK == resultCode){
            Estados estado = (Estados)data.getSerializableExtra("estados");
            savedEstado = estado;
            id = estado.getId_estado();
            edtNombre.setText(estado.getNombre());
        }else
            limpiar();
    }
    public void limpiar() {
        edtNombre.setText("");
        edtNombre.setError(null);
    }

    public void agregarDatos() {

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias",MODE_PRIVATE);

        if(sharedPreferences.getBoolean("primeraVez",true)){

            String estadosRep[] = {"Sinaloa", "Chihuahua", "Sonora", "Yucatán", "Veracruz", "Nuevo León", "Jalisco", "Hidalgo", "Querétaro", "Nayarit"};
            EstadoPersistencia source = new EstadoPersistencia( MainActivity.this);
            source.openDatabase();
            Estados estados = new Estados();
            if(source.allContactos().isEmpty()){
                for (int i = 0; i < 10; i++) {
                    estados.setNombre(estadosRep[i]);
                    source.insertContacto(estados);
                }
                source.close();
            }

            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putBoolean("primeraVez",false);
            editor.commit();

        }


    }
}
