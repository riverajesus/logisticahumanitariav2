package com.example.logisticahumanitaria;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import database.EstadoPersistencia;
import database.Estados;

public class ListaActivity extends ListActivity {
    private EstadoPersistencia estadosPersistencia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Button btnNuevo = findViewById(R.id.btnNuevo);
        estadosPersistencia = new EstadoPersistencia(this);
        estadosPersistencia.openDatabase();
        ArrayList<Estados> estados = estadosPersistencia.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.layout_estado, estados);
        setListAdapter(adapter);

        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = view.findViewById(R.id.lblNombreEstado);
            Button modificar = view.findViewById(R.id.btnModificar);
            Button borrar = view.findViewById(R.id.btnBorrar);

            lblNombre.setText(objects.get(position).getNombre());

            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    estadosPersistencia.openDatabase();
                    estadosPersistencia.deleteContacto(objects.get(position).getId_estado());
                    estadosPersistencia.close();
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estados", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

}

